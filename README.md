# DriftMachine
DriftMachine is a simple iteration program that demonstrates genetic drift in variations.

This project is licenses AGPL v3 (see https://www.gnu.org/licenses/agpl-3.0.en.html)
Copyright 2022 Dave Loper

