# Copyright 2022 Dave Loper. License AGPLv3
# Please visit: https://gitlab.com/daveloper.net/driftmachine

import random

objects = 40
types = 2
iterations = 20

typelist = ["Yellow","White","Red","Green","Orange","Blue","Violet","Brown","Black","Grey"]
array=[]


def showTutorial():
    pass

def setObjectNumber():
    global objects
    objects = int(input(f"Specify new number of objects (currently {objects}): "))
    initArray()


def setTypesNumber():
    global types
    temp = int(input(f"Specify new number of types between 2 and 10 (currently {types}): "))
    if 2 <= temp <= 10:
        types = temp
    initArray()

def setIterations():
    global iterations
    temp = int(input(f"Specify new number of iterations between 1 and 12345678 (currently {iterations}): "))
    if 1 <= temp <= 12345678:
        iterations = temp
    initArray()

def runSimulation(array):
    global iterations
    global objects
    count=iterations+1
    data1=""
    array1=[]
    #print(array)
    data1 = displayData(array)
    print()
    print("===============================")
    print("Starting Generation 0: ", data1)
    for i in range(1,count):                # counts the iterations
        for j in range(objects):            # counts the number of objects. we are trying to reset the array
            item=random.choice(array)
            array1.append(item)
        #print("# Array rerolled")
        #print(array)
        #print("################")
        array1.sort()
        data1=displayData(array1)
        print(f"Starting Generation {i}: ", data1)
        array=array1
        array1=[]
        

def initArray():
    global objects
    global types
    global array
    temparray0=[]
    temparray1=[]
    for i in range(0,objects,types):
        for j in range(types):
            temparray0.append(typelist[j])
    for k in range(objects):
        temparray1.append(temparray0[k])
    temparray1.sort()
    #print(temparray1)
    #displayData(temparray1)
    array=temparray1

def displayData(array):
    #print(array)
    items=[]
    j=""
    count=0
    for i in array:
        if i != j:
            items.append(i)
            j = i
    print(items)
    lists=[]
    for k in items:
        l=countX(array,k)
        list=[k,l]
        lists.append(list)
        #print("debug")
        #print(lists)
        #print("===")
    return(lists)
        

def countX(lst, x):
    count = 0
    for ele in lst:
        if (ele == x):
            count = count + 1
    return count



def menu():
    global objects
    global types
    global iterations
    print()
    print("Menu: driftmachine.py")
    print()
    print(f"     current (o)bjects: {objects}")
    print(f"     number of object t(y)pes: {types}")
    print(f"     number of object (i)terations: {iterations}")
    print("     re(s)et defaults")
    print("     (r)un simulation and report")
    print()
    print("     (l)oad CSV file")
    print("     (t)utorial")
    print()
    print("     (d)isplay data")
    print()
    print("     (q)uit (e)xit")
    print()

def resetDefault():
    global objects
    global types
    global iterations
    objects = 40
    types = 2
    iterations = 20
    initArray()

def main():
    global array
    initArray()
    while True:
        menu()
        command = ''
        command = input("Main Menu - Specify a command: ")
        if (command == "t"):
            showTutorial()
        elif (command == "o"):
            setObjectNumber()
        elif (command == "i"):
            setIterations()
        elif (command == "y"):
            setTypesNumber()
        elif (command == "r"):
            runSimulation(array)
        elif (command == "s"):
            resetDefault()
        elif (command == "q") or (command == "e"):
            print("Goodbye")
            print()
            break
        else:
            print("Not a valid command. Please try again.\n")


if __name__ == "__main__":
        main()
